import { Form, Button } from 'react-bootstrap';
import { Fragment, useState, useContext } from 'react';
import {useEffect} from 'react';
import Swal from 'sweetalert2';

//React Context
import UserContext from '../UserContext';


export default function Login(){

/*
useContext is a react hook used to unwrap our context. It will return the data passed as values
by a provider(UserContext.Provider component in App.js)
*/
	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [isActive, setIsActive] = useState(true);

	useEffect(()=> {
		if((email !== '' && password1 !== '')){
				setIsActive(true);
		}else{
				setIsActive(false);
		}
	}, [email, password1])

	function logInUser(e){
		e.preventDefault();

		setEmail('');
		setPassword1('');
		Swal.fire({
			title: 'Yaaay!!',
			icon: 'Success',
			text: 'You are now logged in!'
		})

		//
		localStorage.setItem('email',email);
		setUser({email: email})

		setEmail('')
		setPassword1('')
	}
	return(
		<Fragment>
		<Form onSubmit={(e) => logInUser(e)}>
		<h1>LogIn</h1>
			<Form.Group>
				<Form.Label>Email Address: </Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange = {e => setEmail(e.target.value)}
					required
				 />
				 <Form.Text className="text-muted">
				 	We'll never share your email with anyone else.</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Enter your Password"
					value={password1}
					onChange = {e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>
			{isActive ?
			<Button variant ="primary" type="submit" id="submitBtn">LogIn</Button>
			:
			<Button variant ="primary" type="submit" id="submitBtn" disabled>LogIn</Button>
		}
		</Form>
		</Fragment>
		)
}