import Banner from '../Components/Banner';
import Highlights from '../Components/Highlights';
import {Fragment} from 'react';
import Welcome from '../Components/Welcome';

export default function Home(){
	return (
		<Fragment>
			<Welcome name="Peter" height="20ft"/>
			<Banner />
			<Highlights />
		</Fragment>	
		)
}
